add_input_file_links()

dune_add_test(NAME test_ffpm
              LABELS multidomain
              SOURCES main.cc
              COMPILE_DEFINITIONS NONISOTHERMAL=0
              CMAKE_GUARD HAVE_UMFPACK
              )

dune_add_test(NAME test_ffpm_ni
              LABELS multidomain nonisothermal
              SOURCES main.cc
              COMPILE_DEFINITIONS NONISOTHERMAL=1
              CMAKE_GUARD HAVE_UMFPACK
              )
