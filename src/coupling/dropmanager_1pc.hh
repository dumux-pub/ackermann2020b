// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StokesDropsDarcyCoupling
 * \copydoc Dumux::StokesDropsDarcyDropManager
 */

#ifndef DUMUX_STOKES_DROPS_DARCY_DROPMANAGER_HH
#define DUMUX_STOKES_DROPS_DARCY_DROPMANAGER_HH

#include <dumux/discretization/localview.hh>
#include <dumux/discretization/elementsolution.hh>

namespace Dumux {

/*!
 * \ingroup StokesDropsDarcyCoupling
 * \brief Drop manager to evaluate, update and store drop information.
 *
 * Implementation for one pore class (homogeneous porous medium).
 */
template<class MDTraits, class CouplingManager>
class StokesDropsDarcyDropManager
{
    using Scalar = typename MDTraits::Scalar;

public:
    static constexpr auto stokesIdx = typename MDTraits::template SubDomain<0>::Index();
    static constexpr auto stokesFaceIdx = typename MDTraits::template SubDomain<1>::Index();
    static constexpr auto interfaceIdx = typename MDTraits::template SubDomain<2>::Index();
    static constexpr auto darcyIdx = typename MDTraits::template SubDomain<3>::Index();

private:
    // obtain the type tags of the sub problems
    using StokesTypeTag = typename MDTraits::template SubDomain<0>::TypeTag;
    using InterfaceTypeTag = typename MDTraits::template SubDomain<2>::TypeTag;
    using DarcyTypeTag = typename MDTraits::template SubDomain<3>::TypeTag;

    using InterfaceIndices = typename GetPropType<InterfaceTypeTag, Properties::ModelTraits>::Indices;
    enum { liquidPhaseIdx = 0 }; // TODO get from fluidsystem

    using StokesGridVariables = GetPropType<StokesTypeTag, Properties::GridVariables>;
    using InterfaceGridVariables = GetPropType<InterfaceTypeTag, Properties::GridVariables>;
    using DarcyGridVariables = GetPropType<DarcyTypeTag, Properties::GridVariables>;
    using SolutionVector = typename MDTraits::SolutionVector;
    using NumEqVector = GetPropType<InterfaceTypeTag, Properties::NumEqVector>;
    using DarcyFVGridGeometry = GetPropType<DarcyTypeTag, Properties::FVGridGeometry>;

    using DarcyGridView = GetPropType<DarcyTypeTag, Properties::GridView>;
    enum { dim = DarcyGridView::dimension };

    struct PoreClass
    {
        // defined by input params/grid:
        Scalar meanPoreRadius;
        Scalar ratio;
        Scalar poreArea;
        int nPores;
        // depend on drop processes:
        Scalar dropVolume;
        Scalar dropRadius;
        Scalar surfaceArea; // surface area of the current drop (for evaporation)
    };

    struct DropDomainInfo
    {
        int domainIdx;
        Scalar domainArea;
        bool dropsHaveFormedInCurrentTimeStep;
        bool dropsHaveDetached;
        bool upscaledDropExists; // true if drops have formed in any pore class in the previous time step
        Scalar upscaledDropVolume; // PC.dropVolume * PC.nPores
        Scalar detachedDropVolume; // sum of all detached drop volumes in domain
        Scalar dropMass;
        PoreClass poreClass; // contains current state for the pore class
    };

public:
    //! Constructor
    StokesDropsDarcyDropManager(const CouplingManager& couplingmanager): couplingManager_(couplingmanager)
    { }

    //! Initializes the drop manager
    template<class DarcyGridVariables>
    void init(DarcyGridVariables& darcyGridVars)
    {
        name_ = getParam<std::string>("Vtk.OutputName");
        outputElem_ = getParam<int>("Output.OutputElement");

        // for debugging
        std::string outputFile = "output_";
        outputFile += name_;
        outputFile += ".csv";
        output_.open(outputFile, std::ios_base::app);

        const auto interfaceSpatialParams = couplingManager_.problem(interfaceIdx).spatialParams();

        // get drop specific and spatial parameters
        contactAngle_ = interfaceSpatialParams.contactAngle() * M_PI / 180; // converted to radians

        // get drop domain information from interface problem
        numberOfDropDomains_ = getParam<int>("Interface.Grid.Cells");
        dropDomains_.resize(numberOfDropDomains_);
        computeDropDomains(couplingManager_.problem(interfaceIdx).fvGridGeometry());

        vMax_ = getParam<Scalar>("Stokes.Problem.Velocity"); // maximum free-flow velocity
        height_ = getParam<std::vector<Scalar>>("Stokes.Grid.Positions1")[1] - getParam<std::vector<Scalar>>("Stokes.Grid.Positions1")[0];
        dragCoeffEq_ = getParam<int>("SpatialParams.DragCoefficientEquation");

        const Scalar numScv = couplingManager_.problem(interfaceIdx).fvGridGeometry().numScv();
        aLiquid_.resize(numScv);
        std::fill(aLiquid_.begin(), aLiquid_.end(), 0.0);
        aGas_.resize(numScv);
        std::fill(aGas_.begin(), aGas_.end(), 1.0);
        aSurface_.resize(numScv);
        std::fill(aSurface_.begin(), aSurface_.end(), 0.0);
        saturation_.resize(numScv);
        std::fill(saturation_.begin(), saturation_.end(), 0.0);

        const Scalar contactAngleHysteresis = 20 * M_PI / 180; // converted to radians
        // Cho et al 2012:
        retentionForce_ = 2 * M_PI * std::pow(std::sin(contactAngle_),2) * std::sin(contactAngleHysteresis/2.0);

        idxOffsetFF_ = getParam<int>("Stokes.Grid.OffsetCellsLeft");
        // compute the index offset between interface and porous medium elements
        const int darcyCells = getParam<int>("Darcy.Grid.Cells0") * getParam<int>("Darcy.Grid.Cells1");
        idxOffsetPM_ = darcyCells - numberOfDropDomains_;

        // compute distance between top two rows of Darcy cell centers for pressure gradient towards interface
        auto darcyFvGridGeometry = localView(darcyGridVars->fvGridGeometry());
        const int numDarcyCells = getParam<Scalar>("Darcy.Grid.Cells0") * getParam<Scalar>("Darcy.Grid.Cells1");
        auto element = darcyGridVars->fvGridGeometry().element(numDarcyCells-1);
        darcyFvGridGeometry.bind(element);
        const Scalar upperElementCenter = (*scvs(darcyFvGridGeometry).begin()).center()[1];
        element = darcyGridVars->fvGridGeometry().element(numDarcyCells-1 - getParam<Scalar>("Interface.Grid.Cells"));
        darcyFvGridGeometry.bind(element);
        const Scalar lowerElemenCenter = (*scvs(darcyFvGridGeometry).begin()).center()[1];
        deltaY_ = upperElementCenter - lowerElemenCenter;

        std::string outputDropVolumesFile = "dropVolumes_";
        outputDropVolumesFile += name_;
        outputDropVolumesFile += ".csv";
        outputDropVolumes_.open(outputDropVolumesFile, std::ios_base::app);
        outputDropVolumes_ << "time V_drop d_drop m_drop" << std::endl;

        std::string outputSaturationFile = "saturationIF_";
        outputSaturationFile += name_;
        outputSaturationFile += ".csv";
        outputSaturation_.open(outputSaturationFile, std::ios_base::app);
        outputSaturation_ << "time SlIF" << std::endl;

        std::string outputForcesFile = "forces_";
        outputForcesFile += name_;
        outputForcesFile += ".csv";
        outputForces_.open(outputForcesFile, std::ios_base::app);
        outputForces_ << "time Fdrag Fret" << std::endl;

        std::string outputFluxesFile = "fluxes_";
        outputFluxesFile += name_;
        outputFluxesFile += ".csv";
        outputFluxes_.open(outputFluxesFile, std::ios_base::app);
        outputFluxes_ << "time pmFlux ffFlux Vdrop" << std::endl;

        timeStep_ = 1;
    }

    //! Store current time step size
    void setTimeStepSize(const Scalar timeStepSize) const
    { timeStepSize_ = timeStepSize; }

    /*!
     * \brief Assign each element to a 'global' drop domain.
     *
     * Called once (before the time loop).
     */
    template<class InterfaceFVGridGeometry>
    void computeDropDomains(const InterfaceFVGridGeometry& fvGridGeometry)
    {
        const int numberOfElements = fvGridGeometry.numScv();
        auto fvGeometry = localView(fvGridGeometry);
        const int numberOfPores = getParam<int>("SpatialParams.NumberOfPores", 0);

        for (int elementIdx = 0; elementIdx < numberOfElements; ++elementIdx)
        {
            auto element = fvGridGeometry.element(elementIdx);
            fvGeometry.bind(element);
            dropDomains_[elementIdx].domainIdx = elementIdx;
            dropDomains_[elementIdx].domainArea = (*scvs(fvGeometry).begin()).volume()
                                                     * getParam<Scalar>("Darcy.Problem.ExtrusionFactor");

            // get pore class information
            std::vector<Scalar> poreClassInformation(0.0);
            poreClassInformation.resize(2);
            poreClassInformation = getParam<std::vector<Scalar>>("SpatialParams.PoreClassInformation");

            // initialize pore class information
            dropDomains_[elementIdx].poreClass.meanPoreRadius = poreClassInformation[0];
            dropDomains_[elementIdx].poreClass.ratio = poreClassInformation[1];
            dropDomains_[elementIdx].poreClass.poreArea = M_PI * std::pow(dropDomains_[elementIdx].poreClass.meanPoreRadius, 2);

            if (numberOfPores != 0) // number of pores set in input params
            {
                dropDomains_[elementIdx].poreClass.nPores = numberOfPores;
                porosity_ = dropDomains_[elementIdx].poreClass.nPores * dropDomains_[elementIdx].poreClass.poreArea
                            / dropDomains_[elementIdx].domainArea;
            }
            else // compute number of pores via porosity
            {
                porosity_ = couplingManager_.problem(interfaceIdx).spatialParams().porosityPM();
                dropDomains_[elementIdx].poreClass.nPores = dropDomains_[elementIdx].domainArea * porosity_ * dropDomains_[elementIdx].poreClass.ratio
                                                            / dropDomains_[elementIdx].poreClass.poreArea;
            }

            waterFluxes_[elementIdx] = 0.0;
            evapFluxes_[elementIdx] = 0.0;
        }
    }

    void update(const SolutionVector& sol, const StokesGridVariables& stokesGridVars,
                const InterfaceGridVariables& interfaceGridVars, const DarcyGridVariables& darcyGridVars) const
    {
        time_ += timeStepSize_;
        output_ << "\n--- Time step " << timeStep_ << ", time: " << time_ << std::endl;

        for (auto& dropDomain : dropDomains_)
        {
            // reset information that has already been evaluated in the current time step
            dropDomain.dropsHaveFormedInCurrentTimeStep = false;
            if (dropDomain.dropsHaveDetached)
            {
                updateDropDomainInfo(dropDomain);
                resetPoreClassInfo(dropDomain.poreClass);
                dropDomain.dropsHaveDetached = false;
            }

            if (!dropDomain.upscaledDropExists) // check drop formation
                checkDropFormation(dropDomain, interfaceGridVars, darcyGridVars, sol);
            else // compute drop growth and check detachment
            {
                // evaluate drop growth due to current fluxes from PM to IF (from couplingdata)
                const Scalar liquidDensityPM = darcyGridVars.curGridVolVars().volVars(idxOffsetPM_ + dropDomain.domainIdx).density(liquidPhaseIdx);
                if (!dropDomain.dropsHaveFormedInCurrentTimeStep)
                    computeDropGrowth(dropDomain, liquidDensityPM); // only if drops have formed in previous time step

                // check if grown drops will detach in next time step
                if (!dropDomain.dropsHaveFormedInCurrentTimeStep)
                    checkDropDetachment(dropDomain, sol, stokesGridVars, interfaceGridVars);
                else // DD.dropsHaveFormedInCurrentTimeStep = true
                    dropDomain.dropsHaveFormedInCurrentTimeStep = false; // reset
            }

            // get saturation for output
            saturation_[dropDomain.domainIdx] = interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).saturation(liquidPhaseIdx);

        } // end for dropDomains_

        timeStep_++;
   } // end update(...)

    //! Checks if any drops form due to current pressure conditions and fluxes
    void checkDropFormation(DropDomainInfo& dropDomain,
                            const InterfaceGridVariables& interfaceGridVars,
                            const DarcyGridVariables& darcyGridVars,
                            const SolutionVector& sol) const
    {
        const Scalar gasPressureIF = interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).pressure(1-liquidPhaseIdx);
        const Scalar liquidPressurePM = darcyGridVars.curGridVolVars().volVars(idxOffsetPM_ + dropDomain.domainIdx).pressure(liquidPhaseIdx);
#if EXP
        const Scalar surfaceTension = 0.0681;
#else
        const Scalar temperature = interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).temperature();
        const Scalar surfaceTension = interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).surfaceTension(temperature);
#endif

        if (dropDomain.domainIdx == outputElem_)
            output_ << "Check drop formation: plPM = " << liquidPressurePM
                    << ", pe+pg = " << -2.0 * surfaceTension * std::cos(contactAngle_) / dropDomain.poreClass.meanPoreRadius + gasPressureIF
                    << std::endl;

        // formation condition
        if (liquidPressurePM >= -2.0 * surfaceTension * std::cos(contactAngle_) / dropDomain.poreClass.meanPoreRadius + gasPressureIF
                && !dropDomain.upscaledDropExists)
        {
            if (dropDomain.domainIdx == outputElem_)
                output_ << "Drops would form due to pressure!" << std::endl;
            dropDomain.dropsHaveFormedInCurrentTimeStep = true;

            const auto darcyVolVars = darcyGridVars.curGridVolVars().volVars(idxOffsetPM_ + dropDomain.domainIdx);

            // compute current flux in PM towards IF
            const auto pLower = darcyGridVars.curGridVolVars().volVars(idxOffsetPM_ + dropDomain.domainIdx - dropDomains_.size()).pressure(liquidPhaseIdx);
            const auto rhoLower = darcyGridVars.curGridVolVars().volVars(idxOffsetPM_ + dropDomain.domainIdx - dropDomains_.size()).density(liquidPhaseIdx);
            const auto mobLower = darcyGridVars.curGridVolVars().volVars(idxOffsetPM_ + dropDomain.domainIdx - dropDomains_.size()).mobility(liquidPhaseIdx);

            const Scalar gradP = (liquidPressurePM - pLower) / deltaY_;
            const Scalar gravity = couplingManager_.problem(darcyIdx).gravity()[1];

            const Scalar vlPM = -1.0 * darcyVolVars.permeability() * mobLower * (gradP - gravity * rhoLower);
            const Scalar currentPmFlux = rhoLower * vlPM * dropDomain.domainArea; // in kg/s

            if (dropDomain.domainIdx == outputElem_)
                output_ << "CurrentPmFlux = " << currentPmFlux
                        << ", plPM = " << liquidPressurePM
                        << ", pLower = " << pLower
                        << ", deltaP = " << liquidPressurePM - pLower
                        << ", vlPM = " << vlPM
                        << std::endl;

            if (vlPM > 0.0) // drop forms
            {
                if (dropDomain.domainIdx == outputElem_)
                    output_ << "Drop forms because vlPM > 0!" << std::endl;

                dropDomain.upscaledDropExists = true;

                // update aLiquid (evaluated for flux into drops in next time step)
#if EXP
                aLiquid_[dropDomain.domainIdx] = porosity_;
#else
                aLiquid_[dropDomain.domainIdx] = 1.0; // all pores occupied
#endif
                aGas_[dropDomain.domainIdx] = 0.0; // = 0 if all pores are occupied
#if NONISOTHERMAL
                aSurface_[dropDomain.domainIdx] = aLiquid_[dropDomain.domainIdx];
#endif
                if (dropDomain.domainIdx == outputElem_)
                    output_ << "aLiquid = " << aLiquid_[dropDomain.domainIdx]
                    << ", aGas = " << aGas_[dropDomain.domainIdx]
                    << ", aSurface = " << aSurface_[dropDomain.domainIdx]
                    << std::endl;
            }
        }
    }

    //! Computes drop growth due to current flux from PM to IF
    void computeDropGrowth(DropDomainInfo& dropDomain, const Scalar liquidDensityPM) const
    {
        if (dropDomain.domainIdx == outputElem_)
            output_ << "Compute drop growth..." << std::endl;

        const Scalar fluxPM = waterFluxes_.at(dropDomain.domainIdx) * dropDomain.domainArea; // in kg/s
        Scalar fluxFF = 0.0;

#if NONISOTHERMAL
        if (dropDomain.poreClass.dropRadius > 0.0)
            fluxFF = evapFluxes_.at(dropDomain.domainIdx) * dropDomain.domainArea; // in kg/s, = evapFluxDrop * ASurface * nPores
#endif
        // total mass in drop domain
        dropDomain.dropMass += (fluxPM + fluxFF) * timeStepSize_; // in kg
        // total additional drop volume due to current flux from PM
        const Scalar fluxVolume = (fluxPM + fluxFF) * timeStepSize_ / liquidDensityPM;

        if (dropDomain.domainIdx == outputElem_)
            output_ << "Water flux from PM = " << fluxPM
                    << ", evaporative flux into FF = " << fluxFF
                    << std::endl;

        dropDomain.poreClass.dropVolume += fluxVolume/dropDomain.poreClass.nPores;
        dropDomain.poreClass.dropRadius = std::cbrt( (3.0/M_PI * dropDomain.poreClass.dropVolume)
                                        / ( (1 - std::cos(contactAngle_)) * (1 - std::cos(contactAngle_)) * (2 + std::cos(contactAngle_)) ));
#if NONISOTHERMAL
        dropDomain.poreClass.surfaceArea = M_PI * std::pow(dropDomain.poreClass.dropRadius, 2); // projected area (normal to flux)
        const Scalar aSurfaceTemp = (dropDomain.poreClass.surfaceArea * dropDomain.poreClass.nPores) / dropDomain.domainArea;
        aSurface_[dropDomain.domainIdx] = dropDomain.poreClass.dropRadius < dropDomain.poreClass.meanPoreRadius ? aLiquid_[dropDomain.domainIdx] : aSurfaceTemp;
#endif

        dropDomain.upscaledDropVolume = dropDomain.poreClass.dropVolume * dropDomain.poreClass.nPores;

        if (dropDomain.domainIdx == outputElem_)
            output_ << "Drop has grown: updated drop volume for PC = " << dropDomain.poreClass.dropVolume
                    << ", upscaled drop volume = " << dropDomain.upscaledDropVolume
                    << ", aLiquid = " << aLiquid_[outputElem_]
                    << ", aSurface = " << aSurface_[outputElem_]
                    << ", dropSurfaceArea = " << dropDomain.poreClass.surfaceArea
                    << ", domainArea = " << dropDomain.domainArea
                    << std::endl;
    }

    //! Checks if drops detach in the given drop domain, update drop volume if detached
    void checkDropDetachment(DropDomainInfo& dropDomain,
                             const SolutionVector& solCurrentIter,
                             const StokesGridVariables& stokesGridVars,
                             const InterfaceGridVariables& interfaceGridVars) const
    {
        if (dropDomain.domainIdx == outputElem_)
            output_ << "Check drop detachment..." << std::endl;

        if (vMax_ > 1.0e-8) // detachment due to drag force, vMax must be > 0
        {
            const Scalar density = stokesGridVars.curGridVolVars().volVars(idxOffsetFF_ + dropDomain.domainIdx).density();
            const Scalar viscosity = stokesGridVars.curGridVolVars().volVars(idxOffsetFF_ + dropDomain.domainIdx).viscosity();
            const Scalar Re = (0.5 * vMax_ * height_ * density) / viscosity;

            // choose drag coefficient
            // 1 = Chen, 2 = Clift, 3 = Cho
            Scalar dragCoefficient = 0;
            if (dragCoeffEq_ == 2)
                dragCoefficient = 24 / Re * (1 + 0.1925 * std::pow(Re, 0.63));
            else if (dragCoeffEq_ == 3)
            {
                const Scalar a = 46.247 * std::pow(2 * dropDomain.poreClass.dropRadius / height_, 0.1757);
                const Scalar b = 0.2158 * (2 * dropDomain.poreClass.dropRadius) / height_ - 0.6384;
                dragCoefficient = a * std::pow(Re, b);
            }
            else
                dragCoefficient = 30 / std::sqrt(Re);

            // const Scalar dragCoefficient = 30 / std::sqrt(Re);
            const Scalar vx = couplingManager_.problem(stokesIdx).xVelocity({0, dropDomain.poreClass.dropRadius * ( 1.0 - std::cos(contactAngle_))});

            // compute F_drag - taken from Baber2014a "simple drag force"
            Scalar projectedArea = std::pow(dropDomain.poreClass.dropRadius, 2) / 2.0 * (2* contactAngle_ - std::sin(2*contactAngle_));
            Scalar dragForce = 0.5 * density * vx * vx * dragCoefficient * projectedArea;
#if EXP
            const Scalar surfaceTension = 0.0681;
#else
            const Scalar temperature = interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).temperature();
            const Scalar surfaceTension = interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).surfaceTension(temperature);
#endif

            if (dropDomain.domainIdx == outputElem_)
                outputForces_ << time_ << " " << dragForce << " " << retentionForce_ * surfaceTension * dropDomain.poreClass.dropRadius << std::endl;

            // compare forces
            if (dragForce > retentionForce_ * surfaceTension * dropDomain.poreClass.dropRadius)
            {
                dropDomain.detachedDropVolume = dropDomain.poreClass.dropVolume * dropDomain.poreClass.nPores;
                dropDomain.dropsHaveDetached = true;
            }
        }
    }

    //! Writes drop volumes and saturation for current time step
    void writeOutput(const Scalar time) const
    {
      outputDropVolumes_ << time;
      outputSaturation_ << time;

      for (const auto& dropDomain : dropDomains_)
      {
          outputDropVolumes_ << " " << dropDomain.poreClass.dropVolume;
          outputDropVolumes_ << " " << 2.0 * dropDomain.poreClass.dropRadius;
          outputDropVolumes_ << " " << dropDomain.dropMass; // drop mass in whole domain

          outputSaturation_ << " " << saturation_[dropDomain.domainIdx];
      }
      outputDropVolumes_ << std::endl;
      outputSaturation_ << std::endl;
    }

    void updateDropDomainInfo(DropDomainInfo& dropDomain) const
    {
        dropDomain.upscaledDropVolume = 0.0;
        dropDomain.upscaledDropExists = false;
        dropDomain.detachedDropVolume = 0.0;
        dropDomain.dropMass = 0.0;

        aLiquid_[dropDomain.domainIdx] = 0.0;
        aGas_[dropDomain.domainIdx] = 1.0;
        aSurface_[dropDomain.domainIdx] = 0.0;

        if (dropDomain.domainIdx == outputElem_)
            output_ << "Drop detached: detached drop volume = "
                    << dropDomain.detachedDropVolume
                    << ", drop radius = " << dropDomain.poreClass.dropRadius
                    << ", hDrop = " << dropDomain.poreClass.dropRadius * (1.0 - std::cos(contactAngle_))
                    << ", aLiq = " << aLiquid_[dropDomain.domainIdx]
                    << std::endl;
    }

    //! Resets pore class information when drop detaches
    void resetPoreClassInfo(PoreClass& poreClass) const
    {
        poreClass.dropVolume = 0.0;
        poreClass.dropRadius = 0.0;
        poreClass.surfaceArea = 0.0;
    }

    NumEqVector detachedDrops(const int elementIndex, const Scalar density, const Scalar massFrac, const Scalar internalEnergy) const
    {
        NumEqVector detachedDrops(0.0);
        if (dropDomains_[elementIndex].detachedDropVolume != 0)
        {
            const Scalar detachedDropsFlux = dropDomains_[elementIndex].detachedDropVolume * density / timeStepSize_; // in kg/s
            detachedDrops[0] = detachedDropsFlux * massFrac;
            detachedDrops[1] = detachedDropsFlux * (1.0 - massFrac);
#if NONISOTHERMAL
            // detached energy
            detachedDrops[2] = dropDomains_[elementIndex].detachedDropVolume * density * internalEnergy / timeStepSize_; // in J/s
#endif
        }
        return detachedDrops;
    }

    const Scalar getDropVolume(const int elementIdx) const
    { return dropDomains_[elementIdx].upscaledDropVolume; }

    Scalar aLiquid(const int elementIndex) const
    { return aLiquid_[elementIndex]; }

    Scalar aGas(const int elementIndex) const
    { return aGas_[elementIndex]; }

    Scalar aSurface(const int elementIndex) const
    { return aSurface_[elementIndex]; }

    Scalar porosityIF() const
    { return porosity_; }

    template<class Flux>
    void setCurrentWaterFlux(const int elementIdx, const Flux& waterFlux) const
    { waterFluxes_.at(elementIdx) = waterFlux; }

    template<class Flux>
    void setCurrentEvapFlux(const int elementIdx, const Flux& evapFlux) const
    { evapFluxes_.at(elementIdx) = evapFlux; }

private:
    const CouplingManager& couplingManager_;

    int numberOfDropDomains_;
    mutable std::vector<DropDomainInfo> dropDomains_;
    std::vector<PoreClass> poreSizeDistribution_;
    mutable std::vector<Scalar> aLiquid_; // area fraction available for liquid flux (liquid-filled pores)
    mutable std::vector<Scalar> aGas_; // area fraction available for gas flux (gas-filled pores)
    mutable std::vector<Scalar> aSurface_; // area fraction available for evaporative flux (liquid-filled pores)
    mutable std::vector<Scalar> saturation_;
    Scalar porosity_;
    Scalar contactAngle_;
    Scalar retentionForce_;
    Scalar vMax_;
    Scalar height_;
    int idxOffsetPM_, idxOffsetFF_;
    Scalar deltaY_;
    int dragCoeffEq_;
    mutable std::unordered_map<std::size_t, Scalar> waterFluxes_;
    mutable std::unordered_map<std::size_t, Scalar> evapFluxes_;

    mutable Scalar timeStepSize_;

    mutable std::ofstream outputDropVolumes_;
    mutable std::ofstream outputSaturation_;
    mutable std::ofstream output_;
    mutable std::ofstream outputForces_;
    mutable std::ofstream outputFluxes_;
    std::string name_;
    int outputElem_;
    mutable int timeStep_;
    mutable Scalar time_;
};

} // end namespace Dumux

#endif
