// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidmatrixinteractions
 */
#ifndef DROPS_MATERIAL_LAW_HH
#define DROPS_MATERIAL_LAW_HH

#include "materiallawparams.hh"

#include <algorithm>

#include <dumux/common/spline.hh>

namespace Dumux {

/*!
 * \ingroup Fluidmatrixinteractions
 */
template <class ScalarT, class ParamsT = DropMaterialLawParams<ScalarT> >
class DropMaterialLaw
{

public:
    using Params = ParamsT;
    using Scalar = typename Params::Scalar;

    /*!
     * \brief Capillary pressure-saturation curve
     */
    static Scalar pc()
    { return 0.0; }

    /*!
     * \brief Returns the relative permeability of the wetting phase (gas)
     */
    static Scalar krw(const Scalar sl, const Scalar mug, const Scalar mul)
    {
#if !LATERAL
        return 0.0;
#else
        return sl > 0.5 ? (1- sl*sl) * (2*(sl+2) - 3*(mul*(1-sl*sl)+mug*sl*sl)/(mul*(1-sl)+ mug*sl)) : 0.0;
#endif
    }

    /*!
    * \brief Returns the relative permeability of the non-wetting phase (liquid)
     */
    static Scalar krn(const Scalar sl, const Scalar mug, const Scalar mul)
    {
#if !LATERAL
        return 0.0;
#else
        return sl > 0.5 ? 4*std::pow(sl,3) : 0.0;
#endif
    }
};

} // end namespace Dumux

#endif
