# dune-common
# releases/2.6 # 1b45bfb24873f80bfc7b419c04dea79f93aef1d7 # 2019-03-19 10:11:58 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.6
git reset --hard 1b45bfb24873f80bfc7b419c04dea79f93aef1d7
cd ..

# dune-geometry
# releases/2.6 # 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38 # 2018-12-06 22:56:21 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.6
git reset --hard 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38
cd ..

# dune-localfunctions
# releases/2.6 # ee794bfdfa3d4f674664b4155b6b2df36649435f # 2018-12-06 23:40:32 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.6
git reset --hard ee794bfdfa3d4f674664b4155b6b2df36649435f
cd ..

# dune-grid
# releases/2.6 # 76f18471498824d49a6cecbfba520b221d9f79ca # 2018-12-10 14:35:11 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.6
git reset --hard 76f18471498824d49a6cecbfba520b221d9f79ca
cd ..

# dune-istl
# releases/2.6 # 9698e497743654b6a03c219b2bdfc27b62a7e0b3 # 2018-12-14 10:00:30 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.6
git reset --hard 9698e497743654b6a03c219b2bdfc27b62a7e0b3
cd ..

# dumux
# releases/3.0 # ae8ae87ddc39d02795ef11ad5744c8e85f334a53 # 2019-02-25 11:32:23 +0100 # Timo Koch
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/3.0
git reset --hard ae8ae87ddc39d02795ef11ad5744c8e85f334a53
cd ..

